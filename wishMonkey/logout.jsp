<%-- 
    Document   : logout
    Created on : Dec 21, 2015, 7:11:52 PM
    Author     : webastral
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
   session.invalidate();
   response.sendRedirect("login.jsp");
 %>

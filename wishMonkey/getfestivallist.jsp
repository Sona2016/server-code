<%@page import="org.json.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.File"%>
<%@include file="Connection.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% // http://112.196.16.94:8080/wishMonkey/getlist.jsp%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>wishMonkey</title>
          <link type="text/css" href="design.css" rel="stylesheet"/>
    </head>
    <body class="body">
         
         <%   
         if(session.getAttribute("email")==null)
         {
           response.sendRedirect("login.jsp");
         } 
         %>
       
         <br><br>   
         
         
         <b> <center><label style="color: #9A5311; font-size: 20px; font-style: italic;">List of Uploaded Festivals</label></center>
        
             <br><br>
            
                  <center>  
                <table  border="1" width="500">
                    <tr style="background-color: appworkspace; height: 50px">
                        <th>Images</th> 
                        <th>Festival Name</th>
                        <th colspan="3">Action</th>
                    </tr>
                     <%  
                         
                   try
                   {
                   //Class.forName("com.mysql.jdbc.Driver");
		  // Connection connection=DriverManager.getConnection("jdbc:mysql://localhost/wishMonkey","root","redhat");
		   Statement statement=getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                           ResultSet.CONCUR_UPDATABLE);
                   ResultSet rs= statement.executeQuery("SELECT * FROM upload_festivals");                 
                   String image=getMainPath()+"fest_image/";
//                  String root = getServletContext().getRealPath("/");
//                   File path = new File(root + "/wishMonkey/images/");
//                   String image =  path+"";
                   while(rs.next())
                   {   
                            String name=rs.getString("name");
                            
                            String id=rs.getString("id");
                            
                            String image_url=image+rs.getString("id")+rs.getString("ext");
                          
                            %> <tr>
                                <td>
                            <center><img src="<%=image_url%>" alt="image" height="50" width="50" style="padding-bottom: 5px;padding-top: 5px "/><center>
                                </td>
                                <td><center><%=name%></center></td>
                                <td><center><a href="afterlogin.jsp?page=editfestivalform&&id=<%=id%>&&type=image">Edit</a></center></td>
                                <!--<td><center><a href="afterlogin.jsp?page=editfestivalimageform&&id=<%=id%>&&type=image">Edit Image</a></center></td>-->
                                <td><center><a href="deletefestival.jsp?id=<%=id%>&&type=image">Delete</a></center></td>
                            </tr>
                <%   }         
                   }
                   catch(Exception e)
                   {
                       e.printStackTrace();
                   }
            %>   
           </table></center>
               
            </body>
</html>


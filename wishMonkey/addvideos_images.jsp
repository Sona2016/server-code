<%@page import="java.sql.*"%>
<%@include file="Connection.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>wishMonkey</title>
        <script>
         var global = 
         {
             count:1
         };
        function validation()
        {
           if(document.getElementById("dropdownlisttype").value=="select")
           {
               alert("Please Select Upload Type Image or Video");
               return false;
           }
           else if(document.getElementById("dropdownlistcategory").value=="select")
           {
               alert("Please Select Upload Category");
               return false;
           }
           
           for(i=1;i<=global.count;i++)
           {
               var id="fileChooser"+i;
               if(document.getElementById(id).value=="")
               {
                    alert("Please Choose Image");
                    return false;
               }
           }
        }
        function selectFileType()
        {
            if(document.getElementById("dropdownlisttype").value=="image")
            {
                for(i=1;i<=global.count;i++)
                {
                    var id="fileChooser"+i;
                    document.getElementById(id).accept="image/*";
                }
            }
            else if(document.getElementById("dropdownlisttype").value=="video")
            {
                for(i=1;i<=global.count;i++)
                {
                    var id="fileChooser"+i;
                    document.getElementById(id).accept="video/*";
                }
            }
        }
        function isFileTypeSelected()
        {
            if(document.getElementById("dropdownlisttype").value=="select")
            {
                alert("Please Select Upload Type Image or Video First");
                return false;
            }
        }
function addnewfilechooser()
{
   //document.getElementById("newdiv").innerHTML = document.getElementById("newdiv").innerHTML+"<br><input type='file' name='data' id='fileChooser'/><br>";
   if(isFileTypeSelected()==false)
   {
       return false;
   }
   global.count=global.count+1;
   var table = document.getElementById("datatable");
   var row = table.insertRow(global.count+2);
   var dtCell = row.insertCell(0);
   if(document.getElementById("dropdownlisttype").value=="image")
   {
        dtCell.innerHTML="<input type='file' name='data' id='fileChooser"+global.count+"' accept='image/*'/>";
    }
    else
    {
        dtCell.innerHTML="<input type='file' name='data' id='fileChooser"+global.count+"' accept='video/*'/>";
    }
};
    </script>
    </head>
    <body>
        
        <!--<input style="background-color: gray; height: 40px;" type="button" onclick="window.location='afterlogin.jsp'" value="GO TO DASHBOARD"/>-->
        <%
            String responsedata=request.getParameter("result");
            if(responsedata==null)
            {
                %>
                <!--<label style="color: black; font-size: 20px; font-style: italic; margin-left: 20px;" id="response">hello</label>-->
            <%
            }
            else
            {
		if(responsedata.equals("1"))
		{
            		%>
                        <script>
                            alertMessage("Image uploaded successfully");
                            window.location='afterlogin.jsp?page=addvideos_images';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("2"))
		{
            		%>
                        <script>
                            alertMessage("Video uploaded successfully");
                            window.location='afterlogin.jsp?page=addvideos_images';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("3"))
		{
            		%>
                        <script>
                            alertMessage("Image deleted successfully");
                            window.location='afterlogin.jsp?page=addvideos_images';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("4"))
		{
            		%>
                        <script>
                            alertMessage("Video deleted successfully");
                            window.location='afterlogin.jsp?page=addvideos_images';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("5"))
		{
            		%>
                        <script>
                            alertMessage("Image updated successfully");
                            window.location='afterlogin.jsp?page=addvideos_images';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("6"))
		{
            		%>
                        <script>
                            alertMessage("Video updated successfully");
                            window.location='afterlogin.jsp?page=addvideos_images';
                       </script>
            		
              		  <%
		}
            }
            %>
            <form action="upload_images_videos.jsp"  method="post" enctype="multipart/form-data">
<!--            <label style="color: black; font-size: 20px; font-style: italic; visibility: hidden" id="responsedata">Result</label>-->
            <table cellspacing="5px" align="center" id="datatable">
                <tr>
                    <td>
                        <b><label style="color: black; font-size: 20px; font-style: italic; ">Upload Images or Videos:</label><b/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select  id="dropdownlisttype" name="dropdowntype" style="width:100%" onchange="selectFileType()">
                        <option value="select" disabled="disabled" selected="selected"> Please Select Type</option>
                        <option id="imagelist" value="image">Images</option>
                        <option id="videolist" value="video">Videos</option></select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select  id="dropdownlistcategory" name="dropdowncategory" style="width:100%">
                        <option value="select" disabled="disabled" selected="selected"> Please Select Category</option>
                        <!--<option id="imagelist" value="New Year">New Year</option>
                        <option id="videolist" value="Birthday">Birthday</option>
                        <option id="videolist" value="Valentine's Day">Valentine's Day</option>
                        <option id="videolist" value="Mother's Day">Mother's Day</option>
                        <option id="videolist" value="Christmas">Christmas</option>
                        <option id="videolist" value="Dussehra">Dussehra</option>
                        <option id="videolist" value="Diwali">Diwali</option></select>-->
                        <%
                            Statement catStatement = getConnection().createStatement();
                            ResultSet catResultSet = catStatement.executeQuery("select id,category from upload_category");
                            while(catResultSet.next())
                            {
                                out.print("<option id ='videolist' value='"+catResultSet.getString("id")+"'>"+catResultSet.getString("category")+"</option>");
                            }
                        %>
                    </td>
                </tr>
                <tr>
                    <td id="newdiv">
                        <input type="file" name="dataFile1" id="fileChooser1" onclick="return isFileTypeSelected()" /><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Upload" style="width:48%" onclick="return validation()" />
                        <input type="button" value="Add More" style="width:48%; float: right" id="addmore"  onclick="addnewfilechooser()"/>
                    </td>
                </tr>
            </table>
            <jsp:include page="getlist.jsp" />
           
            </form>
   

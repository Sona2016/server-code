
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>wishMonkey</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

        <link type="text/css" href="design.css" rel="stylesheet"/>
        <script>
            function addnewfilechooser()
            {
                document.getElementById("newdiv").innerHTML = document.getElementById("newdiv").innerHTML + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
   \n\
 <input type='text' name='Festival Name' id='fest_name' placeholder='Festival Name' style='margin-left: 10px;   color: gray' />\n\
 <font color='white'> <input style='margin-left: 90px;  color: white' type='file' name='data' id='fileChooser'/></font></br></br>";
            }
            ;
        </script>

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });
        </script>

    </head>
    <body class="body">
       
        <%
            if (session.getAttribute("email") == null) {
                response.sendRedirect("login.jsp");
            }
            String responsedata = request.getParameter("result");
            if (responsedata != null) {
                if(responsedata.equals("1"))
		{
            		%>
                        <script>
                            alertMessage("Festival added successfully");
                            window.location='afterlogin.jsp?page=addfestivals';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("2"))
		{
            		%>
                        <script>
                            alertMessage("Festival deleted successfully");
                            window.location='afterlogin.jsp?page=addfestivals';
                       </script>
            		
              		  <%
		}
                else if(responsedata.equals("3"))
		{
            		%>
                        <script>
                            alertMessage("Festival updated successfully");
                            window.location='afterlogin.jsp?page=addfestivals';
                       </script>
            		
              		  <%
		}
            }
                %>

    <form action="upload_festival_n_images.jsp"  method="post" enctype="multipart/form-data" >
        <center>  
            <div class="upload_div">
                <br><br> 
                <b><label style="color: #ffffff; font-size: 20px;">UPLOAD FESTIVALS</label>
                    <b/><br><br><br>
                    <center> 
                        <div class="filechooser" id="newdiv">
                            <center> 

                                <input type="text" id="datepicker"  placeholder="Date" name = "Date Name" required >
                                <input style=" margin-top:10px; margin-bottom: 10px;  color: gray" type="text" maxlength="25" name="Festival Name" id="fest_name" placeholder="Festival Name" required/>
                                <input style=" margin-top: 10px; margin-bottom: 10px; color: white" type="file" name="dataFile1" id="fileChooser1" required/>
                            </center>
                        </div>
                    </center> 
                    <br><br>
                    <!--               <input class="button_style" type="button" value="ADD MORE" id="addmore" onclick="addnewfilechooser()"/>
                    -->

                    <input class="button_style" type="submit" value="UPLOAD"/>
                   <!-- <input class="button_style" type="button" value="FESTIVALS LIST" id="addmore" onclick="window.location = 'getfestivallist.jsp'"/> -->
                    <br><br>
                    </div>
                    </center>                         
                    </form>
    <jsp:include page="getfestivallist.jsp" />
                    </body>
                    </html>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.io.File"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="com.xuggle.mediatool.event.IVideoPictureEvent"%>
<%@page import="com.xuggle.xuggler.Global"%>
<%@page import="com.xuggle.mediatool.MediaListenerAdapter"%>
<%!
            
            public static class ImageSnapListener extends MediaListenerAdapter {
            double SECONDS_BETWEEN_FRAMES = 10;
            int mVideoStreamIndex = -1;
            long mLastPtsWrite = Global.NO_PTS;
           // String outputFilePrefix = "/opt/apache-tomcat-8.0.28/webapps/ROOT/wishMonkey/videos/";
            String outputFilePrefix = "/opt/tomcat/webapps/ROOT/wishMonkey/videos/";
             public static List<String> list=new ArrayList<String>();
            long MICRO_SECONDS_BETWEEN_FRAMES =
            (long) (Global.DEFAULT_PTS_PER_SECOND * SECONDS_BETWEEN_FRAMES);
              public void onVideoPicture(IVideoPictureEvent event) {
            if (event.getStreamIndex() != mVideoStreamIndex) {
                // if the selected video stream id is not yet set, go ahead an
                // select this lucky video stream
                if (mVideoStreamIndex == -1) {
                    mVideoStreamIndex = event.getStreamIndex();
                } // no need to show frames from this video stream
                else {
                    return;
                }
            }
            // if uninitialized, back date mLastPtsWrite to get the very first frame
            if (mLastPtsWrite == Global.NO_PTS) {
                mLastPtsWrite = event.getTimeStamp() - MICRO_SECONDS_BETWEEN_FRAMES;
            }
            // if it's time to write the next frame
            if (event.getTimeStamp() - mLastPtsWrite
                    >= MICRO_SECONDS_BETWEEN_FRAMES) {
                String outputFilename = dumpImageToFile(event.getImage());
                list.add(outputFilename);
                // indicate file written
                double seconds = ((double) event.getTimeStamp())
                        / Global.DEFAULT_PTS_PER_SECOND;
                System.out.printf("at elapsed time of %6.3f seconds wrote: %s\n",
                        seconds, outputFilename);
                // update last write time
                mLastPtsWrite += MICRO_SECONDS_BETWEEN_FRAMES;
            }
        }

        private String dumpImageToFile(BufferedImage image) {
            try {
                String outputFilename = outputFilePrefix
                        + System.currentTimeMillis() + ".png";
                ImageIO.write(image, "png", new File(outputFilename));
                return outputFilename;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        
    }
%>
 
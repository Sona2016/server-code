<%@page import="org.json.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.File"%>
<%@include file="Connection.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% // http://112.196.16.94:8080/wishMonkey/getlist.jsp%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>wishMonkey</title>
          <link type="text/css" href="design.css" rel="stylesheet"/>
    </script>
    </head>
    <body>
        
         <%   
         if(session.getAttribute("email")==null)
         {
           response.sendRedirect("login.jsp");
         } 
         %>
       
               
    
       
                <br><br><br>
                <div style="float: left">
                    <label style="color: #9A5311; font-size: 20px; font-style: italic;">List of Uploaded Images</label>
                <table style="" border="1" width="400">
                    <tr style="background-color: appworkspace; height: 50px">
                        <th>Images</td> 
                        <th>Category</td>
                        <th>Action</td>
                    </tr>
                     <%  
                         
                   try
                   {
                   Class.forName("com.mysql.jdbc.Driver");
		  // Connection connection=DriverManager.getConnection("jdbc:mysql://localhost/wishMonkey","root","redhat");
		   Statement statement=getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                           ResultSet.CONCUR_UPDATABLE);
                   ResultSet rs= statement.executeQuery("SELECT uploaded_images.id,image,extension,upload_category.category as category FROM uploaded_images,upload_category where uploaded_images.category=upload_category.id order by uploaded_images.category");                 
                   //String image="http://112.196.16.94:8080/wishMonkey/images/";
		   String image=getImagePath();
                   while(rs.next())
                   {   
                            String category=rs.getString("category");
                            String id=rs.getString("id");
                            String image_url=image+rs.getString("id")+rs.getString("extension");
                            %> <tr>
                                <td>
                            <center><img src="<%=image_url%>" alt="image" height="50" width="50" style="padding-bottom: 5px;padding-top: 5px "/><center>
                                </td>
                                <td><center><%=category%></center></td>
                                <td><center><a href="Editfile.jsp?edit=<%=id%>&&type=image">Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="deletefile.jsp?edit=<%=id%>&&type=image">Delete</a></center></td>
                            </tr>
                <%   }         
                   }
                   catch(Exception e)
                   {
                       e.printStackTrace();
                   }
            %>   
            </table>
            </div>           
            <div style="float: left">
                 <label style="color: #9A5311; font-size: 20px; font-style: italic; margin-left: 80px ">List of Uploaded Videos</label>
                <table style=" margin-left: 80px" border="1" width="400">
                    <tr style="background-color: appworkspace; height: 50px">
                        <th>Videos</td> 
                        <th>Category</td>
                        <th>Action</td>
                    </tr>
                     <%  
                   try
                   {
                   
		   Statement statement=getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                           ResultSet.CONCUR_UPDATABLE);
                   ResultSet rs= statement.executeQuery("SELECT uploaded_videos.id,video,extension,thumbnail,upload_category.category as category FROM uploaded_videos,upload_category where uploaded_videos.category=upload_category.id order by uploaded_videos.category");                 
                  // String image="http://112.196.16.94:8080/wishMonkey/videos/";
		String image=getVideoPath();
                   while(rs.next())
                   {   
                            String id=rs.getString("id");
                            String category=rs.getString("category");
                            String video_url=image+rs.getString("id")+rs.getString("extension");
                            %> <tr>
                                <td>
                            <center>
                                 <EMBED SRC="<%=video_url%>" alt="video thumb" loop="1" height="50" width="50" autostart="false" style="padding-bottom: 5px;padding-top: 5px ">
                                <center>
                                </td>
                                <td><center><%=category%></center></td>
                                <td><center><a href="Editfile.jsp?edit=<%=id%>&&type=video">Edit</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="deletefile.jsp?edit=<%=id%>&&type=video">Delete</a></center></td>
                            </tr>
                <%   }         
                   }
                   catch(Exception e)
                   {
                       e.printStackTrace();
                   }
            %>   
            </table></div>
        
            </body>
</html>

